# README #

### What is this repository for? ###

* Landing Health Care Soft
* 1.0.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Instalar Node.js
* Instalar Git & Git Bash (requerido)
* En Git Bash: `cd [PATH]/Documentos/`
* `git clone git@bitbucket.org:jlopez3573/landing-carolina.git`
* `cd landing-carolina`
* `npm install`
* `npm run start` Para iniciar el servidor en modo `development`
* `localhost:8080` Abrir en el navegador Google Chrome
* Para compilar el proyecto `npm run build`
* La carpeta generada `dist` será la cual debe subirse al servidor
